export const validateForm = (refDom:any) => {
	return (target: any, propertyName: string, descriptor: any) => {
		console.log('装饰器入参', target, propertyName, descriptor);
		const method = descriptor.value;
		descriptor.value = function() {
			console.log(refDom);
			return method.apply(this, arguments);
		};
	};
};
