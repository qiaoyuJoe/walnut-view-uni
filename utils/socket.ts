import { socketUrl } from './config'
import Store from '../store'
import { MsgType } from './utils'

class WebSocket {
  constructor() {
    this.socketTask = null
    this.pingId = null
    this.pongId = null
    this.reconId = null // 重连id
    this.lockReconnect = false
    this.forbidReconnect = false
    this.limitCount = 14
  }

  async createWebSocket() {
		this.lockReconnect = false
		this.forbidReconnect = false
    // 连接
    try {
      this.socketTask = await uni.connectSocket({
        url: `${socketUrl}?token=${Store.getters.getToken}`,
        complete: () => {},
      })
      this.eventHandle()
      console.log(this.socketTask)
    } catch (e) {
      this.reconnect()
    }
  }

  eventHandle() {
    const token = Store.getters.getToken
    this.socketTask.onOpen(() => {
      console.log('连接成功')
			uni.hideLoading()
			this.limitCount = 14
      this.socketTask.send({
        data: JSON.stringify({
          code: MsgType.MSG_READY,
          token,
        }),
      })
      this.heartCheck()
    })
    this.socketTask.onClose(() => {
      console.log('连接关闭')
			this.reconnect()
    })
    this.socketTask.onError(() => {
      console.log('连接异常')
			this.reconnect()
    })
    this.socketTask.onMessage(({ data }) => {
			uni.hideLoading()
      const { code, message } = JSON.parse(data)
      if ([MsgType.MSG_PING, MsgType.MSG_MESSAGE].includes(code)) {
        this.heartCheck()
      }
      switch (code) {
        case MsgType.MSG_MESSAGE: {// 消息
					this.receiveMsg(message)
          break;
				}
        case MsgType.MSG_ONLINE: // 在线 or 离线
          break;
        default:
          break
      }
    })
  }
	
	// 保存消息
	receiveMsg (message) {
		console.log(message)
		const {mine} = message
		if (mine) {
			const params = {
				id,
				friendType: type,
			}
			Api.userAddMyMessageFriend(params)
		} else {
			Store.commit('setUnReadCount', message)
		}
	}
	
	// 主动关闭
	close () {
		this.forbidReconnect = true // 防止 重连
		this.socketTask && this.socketTask.close()
		this.socketTask = null
		this.heartReset()
	}
  // 心跳检车
  heartCheck() {
    this.heartReset()
    this.heartStart()
  }
  heartStart() { // 30s发送 + 15s的回复
	// #ifndef APP-PLUS
  console.table([{ pingId: this.pingId, pongId: this.pongId }])
	// #endif
    this.pingId = setTimeout(() => {
			console.info('发送')
			// #ifndef APP-PLUS
      console.count('发送')
			// #endif
      this.sendPing()
      this.pongId = setTimeout(() => {
        this.reconnect()
      }, 15000)
    }, 30000)
  }
  heartReset() {
    clearTimeout(this.reconId)
    clearTimeout(this.pingId)
    clearTimeout(this.pongId)
  }
  // 重新连接
  reconnect() {
		// #ifndef APP-PLUS
    console.table([{ forbidReconnect: this.forbidReconnect, lockReconnect: this.lockReconnect }])
		// #endif
		console.log('重连次数', this.limitCount)
    if (this.limitCount <= 0) {
      // 请求重新登录接口， 重新连接
			console.log('连接失败')
			uni.hideLoading()
			uni.reLaunch({
				url: '/pages/login/login'
			})
      return
    }
    // 主动拒绝重新连接
    // 防止多次重新连接
    if (this.lockReconnect || this.forbidReconnect) return
		
		uni.showLoading({
			title: '重新连接中'
		})
    this.lockReconnect = true
    this.reconId = setTimeout(() => {
      this.limitCount -= 1
      this.socketTask = null
      this.createWebSocket()
    }, 5000)
  }
  // 发送心跳
  sendPing() {
    this.socketTask.send({
      data: JSON.stringify({
        code: MsgType.MSG_PING,
        token: Store.getters.getToken,
      }),
    })
  }
  // 发送消息
  sendMsg(message) {
    this.socketTask.send({
      data: JSON.stringify({
        code: MsgType.MSG_MESSAGE,
        token: Store.getters.getToken,
        message,
      }),
    })
  }
}

export const webSocket = new WebSocket()
