import Store from '../store'
import { baseUrl } from './config'

export enum ErrorType {
  '401' = '账号或密码错误',
  '400' = '账号或密码错误',
  '504' = '服务超时',
}
export enum MsgType {
  MSG_PING = '0', // 心跳
  MSG_READY = '1', // 就绪
  MSG_MESSAGE = '2', // 消息
  MSG_UPDATE_GROUP = '4', // 修改组
  MSG_ONLINE = '5', // 在线
  MSG_TOKEN_OUT = '6', // 挤掉
}
export const calcDeep = (list) => {
  //  {text, value}
  const _list = []
  list.forEach((el) => {
    const p = {}
    p.text = el.fname
    p.value = el.fno
    if (el.childDepartment && el.childDepartment.length > 0) {
      p.children = calcDeep(el.childDepartment)
    }
    _list.push(p)
  })

  return _list
}

/**
 * hideTime: true 隐藏时间
 *
 * */
export const calcMsgTimestamp = (list = []) => {
  let i = 0
  list.forEach((el) => {
    el.content = transform(el.content)
    if (Math.abs(el.timestamp - i) > 300000) {
      i = el.timestamp
    } else {
      el.hideTime = true
    }
  })
  return list
}

export const newChat = (item) => {
  return {
    ...item,
    sign: item.lastMessage,
  }
}

export const fileReader = (formid) => {
  return new Promise((resolve, reject) => {
    const userId = Store.getters.getUserId
    // #ifdef APP-PLUS
    plus.io.requestFileSystem(
      plus.io.PRIVATE_DOC,
      (fs) => {
        // 成功回调 fs：fileSystem 操文件系统对象
        // root.name doc/
        fs.root.getFile(
          `./${userId}/${formid}.js`,
          { create: true },
          (fileEntry) => {
            fileEntry.file(function (file) {
              let reader = new plus.io.FileReader()
              reader.readAsText(file, 'utf-8')
              reader.onloadend = function (evt) {
                const { target } = evt
                const list = target.result
                  .split('\n')
                  .filter((f) => f)
                  .map((m) => JSON.parse(m))
                resolve(list)
              }
              reader.onerror = function () {
                reject('读取失败')
              }
            })
          },
          () => {
            reject('新建获取文件失败')
          }
        )
      },
      () => {
        reject('请求文件系统失败')
      }
    )
    // #endif
		// #ifndef APP-PLUS
		resolve([])
		// #endif
  })
}

/*
 *seekPoint: 应对插入数据的位置
 */
export const fileWriter = (data, seekPoint = 0, formid) => {
  return new Promise((resolve, reject) => {
    const userId = Store.getters.getUserId
    // #ifdef APP-PLUS
    plus.io.requestFileSystem(
      plus.io.PRIVATE_DOC,
      (fs) => {
        fs.root.getFile(
          `./${userId}/${formid}.js`,
          { create: true },
          (fileEntry) => {
            fileEntry.file(function (file) {
              fileEntry.createWriter(
                (writer) => {
                  console.log('写入成功', '位置', file.size, seekPoint)
                  writer.seek(seekPoint || file.size)
                  if (Array.isArray(data)) {
                    writer.write(
                      data.map((m) => JSON.stringify(m)).join('\n') + '\n'
                    )
                  } else {
                    writer.write(JSON.stringify(data) + '\n')
                  }
                  writer.onerror = function () {
                    reject('写入失败')
                  }
                  writer.onsuccess = function () {
                    resolve('ok')
                  }
                },
                (err) => {
                  reject('创建文件失败')
                }
              )
            })
          },
          (err) => {
            reject('获取文件失败')
          }
        )
      },
      () => {
        reject('请求文件系统失败')
      }
    )
    // #endif
		// #ifndef APP-PLUS
		resolve([])
		// #endif
  })
}

/**
 * 图片加载完成处理函数
 * @param arr 图片的src集合
 * @returns {Promise}
 */
const preloadImages = (arr) => {
  let loadedImage = 0
  let images = []
  return new Promise(function (resolve, reject) {
    for (let i = 0; i < arr.length; i++) {
      images[i] = new Image()
      images[i].src = arr[i]
      images[i].onload = function () {
        loadedImage++
        if (loadedImage === arr.length) {
          resolve()
        }
      }
      images[i].onerror = function () {
        reject()
      }
    }
  })
}

/**
 * 图片加载完成，聊天对话框scroll拉到最下
 * @param id 容器id
 */
export function imageLoad(id) {
  // #ifndef APP-PLUS
  scrollBottom(id)
  let messageBox = document.getElementById(id)
  if (messageBox) {
    let images = messageBox.getElementsByTagName('img')
    if (images) {
      let arr = []
      for (let i = 0; i < images.length; i++) {
        arr[i] = images[i].src
      }
      preloadImages(arr)
        .then(() => {
          scrollBottom(id)
        })
        .catch(function () {
          scrollBottom(id)
        })
    }
  }
  // #endif
  // #ifdef APP-PLUS
  uni
    .createSelectorQuery()
    .select('#' + id)
    .boundingClientRect((data) => {
      //目标节点
      uni.pageScrollTo({
        duration: 0, //过渡时间必须为0，uniapp bug，否则运行到手机会报错
        scrollTop: data.height, //滚动到实际距离是元素距离顶部的距离减去最外层盒子的滚动距离
      })
    })
    .exec()
  // #endif
}

/**
 * 滚动条到最下方
 * @param id 容器id
 */
export const scrollBottom = (id) => {
  let div = document.getElementById(id)
  if (div) {
    uni.pageScrollTo({
      scrollTop: div.scrollHeight,
      duration: 300,
    })
    // div.scrollTop = div.scrollHeight
  }
}

export let faceUtils = {
  alt: [
    '[微笑]',
    '[嘻嘻]',
    '[哈哈]',
    '[可爱]',
    '[可怜]',
    '[挖鼻]',
    '[吃惊]',
    '[害羞]',
    '[挤眼]',
    '[闭嘴]',
    '[鄙视]',
    '[爱你]',
    '[泪]',
    '[偷笑]',
    '[亲亲]',
    '[生病]',
    '[太开心]',
    '[白眼]',
    '[右哼哼]',
    '[左哼哼]',
    '[嘘]',
    '[衰]',
    '[委屈]',
    '[吐]',
    '[哈欠]',
    '[抱抱]',
    '[怒]',
    '[疑问]',
    '[馋嘴]',
    '[拜拜]',
    '[思考]',
    '[汗]',
    '[困]',
    '[睡]',
    '[钱]',
    '[失望]',
    '[酷]',
    '[色]',
    '[哼]',
    '[鼓掌]',
    '[晕]',
    '[悲伤]',
    '[抓狂]',
    '[黑线]',
    '[阴险]',
    '[怒骂]',
    '[互粉]',
    '[心]',
    '[伤心]',
    '[猪头]',
    '[熊猫]',
    '[兔子]',
    '[ok]',
    '[耶]',
    '[good]',
    '[NO]',
    '[赞]',
    '[来]',
    '[弱]',
    '[草泥马]',
    '[神马]',
    '[囧]',
    '[浮云]',
    '[给力]',
    '[围观]',
    '[威武]',
    '[奥特曼]',
    '[礼物]',
    '[钟]',
    '[话筒]',
    '[蜡烛]',
    '[蛋糕]',
  ],
  faces() {
    let self = this
    let arr = {}
    for (let i = 0; i < self.alt.length; i++) {
      arr[self.alt[i]] = '/static/face/' + i + '.gif'
    }
    return arr
  },
}

export const transform = (content) => {
  // const { VUE_APP_UPLOAD_PORT, VUE_APP_IP } = process.env
  let prefix = `${baseUrl}imup/api/download/v1?fileId=`
  // let prefix = 'http://www.baidu.com/'
  // 支持的html标签
  let html = function (end) {
    return new RegExp(
      '\\n*\\[' +
        (end || '') +
        '(code|pre|div|span|p|table|thead|th|tbody|tr|td|ul|li|ol|li|dl|dt|dd|h2|h3|h4|h5)([\\s\\S]*?)]\\n*',
      'g'
    )
  }
  let fa = faceUtils.faces()
  if (content) {
    content = content
      .replace(/&(?!#?[a-zA-Z0-9]+;)/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/'/g, '&#39;')
      .replace(/"/g, '&quot;') // XSS
      .replace(
        /@(\S+)(\s+?|$)/g,
        '@<a href="javascript:;" class="people1">$1</a>$2'
      )

      .replace(/face\[([^\s\\[\]]+?)]/g, function (face) {
        // 转义表情
        let alt = face.replace(/^face/g, '')
        return `<img alt="${fa[alt]}" title="${fa[alt]}" src="${fa[alt]}">`
      })
      .replace(/img\([\s\S]+?\)\[[\s\S]*?]/g, function (img) {
        // 转义图片
        let href = (img.match(/img\(([\s\S]+?)\)\[/) || [])[1]
        let text = (img.match(/\)\[([\s\S]*?)]/) || [])[1]
        if (!href) return img
        return `<img class="message-img" width="100" height="100" title="${text}" src="${prefix + href}" alt="消息图片不能加载">`
      })
      .replace(/file\([\s\S]+?\)\[[\s\S]*?]/g, function (str) {
        // 转义文件
        let href = (str.match(/file\(([\s\S]+?)\)\[/) || [])[1]
        let text = (str.match(/\)\[([\s\S]*?)]/) || [])[1]
        if (!href) return str
        return `<span class="message-file"><span style="display: none">${
          prefix + href
        }</span><span class="arrow-down"></span>${text || href}</span>`
      })
      .replace(/audio\[([^\s]+?)]/g, function (audio) {
        // 转义音频
        return `<div class="message-audio" data-src="${audio.replace(
          /(^audio\[)|(]$)/g,
          ''
        )}"><i class="layui-icon">&#xe652;</i><p>音频消息</p></div>`
      })
      .replace(/video\[([^\s]+?)]/g, function (video) {
        // 转义音频
        return `<div class="message-video"  data-src="${video.replace(
          /(^video\[)|(]$)/g,
          ''
        )}"><i class="layui-icon">&#xe652;</i></div>`
      })
      .replace(/a\([\s\S]+?\)\[[\s\S]*?]/g, function (str) {
        // 转义链接
        let href = (str.match(/a\(([\s\S]+?)\)\[/) || [])[1]
        let text = (str.match(/\)\[([\s\S]*?)]/) || [])[1]
        if (!href) return str
        return `<span class="message-link"><span style="display: none">${href}</span>${text || href}</span>`
      })
      .replace(/card\([\s\S]+?\)\[[\s\S]*?]/g, function (str) {
        // 转义卡片
        let href = (str.match(/card\(([\s\S]+?)\)\[/) || [])[1]
        let text = (str.match(/\)\[([\s\S]*?)]/) || [])[1]
        let arr = text.split(',')
        if (!arr || arr.length === 0) return str
        let items = arr
          .map(
            (m, index) =>
              `<span class="message-card${index}"><span style="display: none">${href}</span>${m}</span>`
          )
          .join('')
        return `<div class="message-card">${items}</div>`
      })
      .replace(html(), '<$1 $2>')
      .replace(html('/'), '</$1>') // 转移HTML代码
      .replace(/\n/g, '<br>') // 转义换行
  }
  return content
}

export const deepClone = (obj) => {
  const objClone = Array.isArray(obj) ? [] : {}
  if (obj && typeof obj === 'object') {
    for (const key in obj) {
      if (obj[key] && typeof obj[key] === 'object') {
        objClone[key] = deepClone(obj[key])
      } else {
        objClone[key] = obj[key]
      }
    }
  }
  return objClone
}
