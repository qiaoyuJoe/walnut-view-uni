export default {
	getToken: (state) => state.userInfo.access_token,
	getAccessToken: (state) => `bearer ${state.userInfo.access_token}`,
	getUserInfo: (state) => state.userInfo,
	getUserId: (state) => state.userInfo.id,
	getChatList: (state) => state.chatList,
	getMessageList: (state) => state.messageList
}