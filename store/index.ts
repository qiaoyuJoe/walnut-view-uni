import { createStore } from 'vuex'
import createPersistedstate from 'vuex-persistedstate'
import { localStorage } from 'mp-storage'
import state from './state'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

const store = createStore({
	state,
	mutations,
	actions,
	getters,
	plugins: [
		createPersistedstate({
			storage: localStorage
		})
	]
})

export default store