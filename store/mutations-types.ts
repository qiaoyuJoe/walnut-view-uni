export default {
	SETUSERINFO: 'setUserInfo',
	SETCHATLIST: 'setChatList',
	DELCHATLIST: 'delChatList',
	TOCHAT: 'toChat',
	QUITOUT: 'quitOut',
	SETMESSAGELIST: 'setMessageList',
	SETUNREADCOUNT :'setUnReadCount'
}

export interface UserInfo {
	username?: string,
	password?: string,
	remember?: number,
	access_token?: string,
	expires_in?: number,
	refresh_token?: string,
	id?: string,
	avatar?: string,
	name?: string,
	sign?: string,
	mobile?: string,
	email?: string,
	loginPhone?: string,
	deptId?: string,
	depts?: string,
	type?: string,
	onlineStatus?: number | null,
	job?: string,
	remarks?: string,
}

export interface Chat {
	id: string,
	name: string,
	avatar: string,
	unReadCount?: number,
	lastMessage?: string,
	sign?: string,
	mobile?: string,
	email?: string,
	type: string,
	deptId: string,
	depts: string,
	remarks?: string,
	onlineStatus: number,
	job: string,
}