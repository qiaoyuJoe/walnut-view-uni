import types, { Chat } from './mutations-types'
import Api from '../api'

export default {
  [types.TOCHAT]({ state, commit }, item: Chat) {
    Api.userAddMyMessageFriend({ id: item.id, friendType: item.type })
    commit(types.TOCHAT, item)
  },
  [types.DELCHATLIST]({ commit }, id) {
    const p = { groupId: id, id }
    Api.userDelMyFriend(p)
    commit(types.DELCHATLIST, id)
  },
}
