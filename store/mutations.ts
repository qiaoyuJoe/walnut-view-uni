import types, { UserInfo, Chat } from './mutations-types'
import { newChat, deepClone, MsgType } from '../utils'

export default {
  [types.SETUSERINFO](state, data: UserInfo) {
    state.userInfo = { ...state.userInfo, ...data }
  },
  [types.SETCHATLIST](state, data) {
    state.chatList = data
  },
  [types.DELCHATLIST](state, id) {
    const index = state.chatList.findIndex((f) => f.id === id)
    state.chatList.splice(index, 1)
  },
  [types.TOCHAT](state, item: Chat) {
    // 到某个页面
    // 添加chatList
    // 清空未读
    const curChat = state.chatList.find((f) => f.id === item.id)
    if (!curChat) {
      const chat = newChat(item)
      state.chatList.unshift(chat)
    } else {
      curChat.unReadCount = 0
    }
    uni.navigateTo({
      url: `/pages/chatBox/chatPage?id=${item.id}`,
    })
  },
  [types.QUITOUT](state) {
    state.userInfo = {
      ...state.userInfo,
      access_token: '',
      expires_in: 0,
      refresh_token: '',
    }
    uni.reLaunch({
      url: '/pages/login/login',
    })
  },
  [types.SETMESSAGELIST](state, messageList) {
    if (messageList.length === 0) {
      state.messageList = []
    } else {
      state.messageList.push(...messageList)
    }
  },
  [types.SETUNREADCOUNT](state, message) {
    const chatList = deepClone(state.chatList)
    const chatIndex = chatList.findIndex(
      (f) =>
        (message.type === '0' && f.id === message.fromid) ||
        (message.type === '1' && f.id === message.id)
    )

    let chat = null
    if (chatIndex === -1) {
      //
      const p = {
        avatar: message.avatar,
        type: message.type,
        remarks: message.remarks,
        unReadCount: 1,
        lastMessage: message.content,
      }
      switch (message.type) {
        case '0':
          p.deptId = message.deptId
          p.depts = message.depts
          p.mobile = message.mobile
          p.email = message.email
          p.onlineStatus = 1
          p.id = message.fromid
          p.name = message.username
          break
        case '1':
          p.id = message.id
          p.name = message.groupname
          break
        default:
          break
      }

      chat = p
    } else {
      chat = chatList.splice(chatIndex, 1)[0]
      chat.unReadCount = +chat.unReadCount + 1
      chat.sign = message.content
      chat.onlineStatus = 1
    }
    chatList.unshift(chat)
    state.chatList = deepClone(chatList)
  },
}
