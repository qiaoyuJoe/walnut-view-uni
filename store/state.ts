
export default {
	userInfo: {
		username: '',
		password: '',
		remember: false, // 记住密码
		access_token: '',
		expires_in: 0,
		refresh_token: '',
		id: "",
		avatar: "",
		name: "",
		sign: "",
		mobile: '',
		email: "",
		loginPhone: '',
		deptId: "",
		depts: "",
		type: '',
		unReadCount: '',
		onlineStatus: 0,
		job: '',
		remarks: '',
	},
	chatList: [],
	messageList: []
}