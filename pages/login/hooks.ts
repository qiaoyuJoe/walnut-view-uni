import { reactive } from 'vue'
import { validateForm } from '../../utils'

export const useFormData = () => {
  const formData = reactive({
    username: '',
    password: '',
  })

  const handleClickSign = (refDom) => {
		console.log(refDom)
  }

  return {
    formData,
    handleClickSign,
  }
}

