import store from '../store'
import {ErrorType, baseUrl} from '../utils'


const apiMapping = {}

interface apiModal {
  path: string,
  header?: any,
  method?: string,
  baseURL?: string,
}

const apiList:Array<apiModal> = [
  {
    path: 'oauth/token',
		baseURL: baseUrl + 'im/',
    header: {
      'content-type': 'application/x-www-form-urlencoded',
    },
  },
  { path: 'register' },
  { path: 'user/init' },
  { path: 'message/addGroup' },
  { path: 'dept/getDeptList', method: 'GET' },
  { path: 'dept/getUserByDeptId' },
  { path: 'group/delGroup', method: 'GET' },
  { path: 'group/outGroup', method: 'GET' },
  { path: 'user/get', method: 'GET' },
  { path: 'group/getMyGroup', method: 'GET' },
  { path: 'user/selectByName' },
	{ path: 'user/chatUserList',
		header: {
			'content-type': 'application/x-www-form-urlencoded',
		}
	},
  { path: 'user/addMyMessageFriend', method: 'GET' },
  { path: 'user/delMyFriend', method: 'GET' },
  { path: 'group/delGroupUser', method: 'GET' },
  { path: 'message/list' },
  { path: 'group/updGroupName', method: 'GET' },
  { path: 'conf/getConf', method: 'GET' },
  // {
  //   path: 'multipart/init',
  //   baseURL: `http://${VUE_APP_IP}:${VUE_APP_UPLOAD_PORT}`,
  // },
  // {
  //   path: 'multipart/complete',
  //   baseURL: `http://${VUE_APP_IP}:${VUE_APP_UPLOAD_PORT}`,
  // },
]
apiList.forEach((el:apiModal) => {
  const name = el.path.replace(/(\/[a-z]{1})/, (a) => a[1].toUpperCase())
  apiMapping[name] = async (data) => {
		const _baseURL = el.baseURL || `${baseUrl}im/api/`
    const config = {
      url: `${_baseURL}${el.path}`,
      method: el.method || 'POST',
      data,
      header: {
				'Accept': 'application/json',
				// 'content-Type': 'application/json',
				'Authorization': store.getters.getAccessToken,
				...(el.header || {})
      },
			withCredentials: true,
    }
    const res = await uni.request(config)
		const { statusCode } = res
		let success = true
		if (statusCode !== 200) {
      uni.showToast({title: ErrorType[statusCode] || '服务器错误', icon: 'none'})
			success = false
		}
		if (res.data.code && res.data.code !== '200') {
			uni.showToast({title: res.data.message, icon:'none' })
			success = false
		}
		
		if (Array.isArray(res.data)) {
			return { result: res.data, success }
		}
		return {...res.data, success}
  }
})

export default apiMapping
