# 务必记得点个赞哦。

# 刚兴趣的同学可以加 Wx:531439864， 有建议或者意见可以留言或者提 issues

## 备注“神秘来宾”并附上 gitee 的用户名。希望我们可以一起构建

# 项目依赖

1. mp-storage 接管 vuex 持久缓存
2. vuex-persistedstate vuex 的持久化插件
3. @dcloudio/uni-app uni-app 的函数插件
4. dayjs 日期格式化
5. scss、sass-resources-loader

# 开发工具

1. HbuilderX
2. vscode
3. git
4. npm
5. cnpm
6. node last 2 version
7. iconfont 图标管理

# 依赖的插件

1. app 真机运行
2. dart-sass 编译
3. Hbuidler English
4. typescript 语言服务
5. uni-app App 调试
6. uni-app（Vue2、vue3） 编译
7. uni-helpers
8. uni-modules 插件
9. 内置终端
10. 内置浏览器

# 功能点

> 1. 文本、表情、图标、文件、链接、通知、文件聊天
> 2. 单聊、群聊
> 3. 部门组织架构、所属群、所在群组
> 4. 支持心跳检测、断线重连、断线提醒、断线重新登录，可加入单点登录
> 5. 用户搜索、拉人进群、踢人出群、转交群主、修改群名称

# 截图

![首页](./screen/49CAA.png)
![消息](./screen/8579C66E-187D-44b5-B11C-7CC686563338.png)
![聊天](./screen/BB23EFE4-1EFA-45d9-9E97-4A239DA3D3F9.png)
![组织](./screen/3239F5A4-C9B4-4f35-AC92-CD3CA75D2BEE.png)
![组织架构](./screen/2ADEDDCF-4A72-4709-B337-0C3F22BF09FE.png)
![组织群聊](./screen/9FA6D521-AA1B-4b5e-820B-8E7DECA40A0B.png)
![个人](./screen/069A951F-4788-45be-9D5B-1BB0615C83FF.png)

# 进度

> 1. 内置浏览器测试基本成功
> 2. 安卓真机调试（loading...）

# 打包

> 目前打包还没测试，处于开发阶段

# 参考项目及技术

> 1.  v-im (主要是表情、文件格式设定)
> 2.  uni-ui 框架
> 3.  界面仿微信， 如有侵权请告知

# 后续目标

> 1.  实现局域内人员的通讯

# 交流授权

> 1. 如果您觉的有帮助到您开发，可以给个 star，或者给个捐赠
> 2. 如果适用于商业用途，请在项目下方给项目进行捐赠，并留下公司名称，否则视为侵权。
